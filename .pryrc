require "awesome_print"
AwesomePrint.pry!

if defined?(Rails)
  host = ActiveRecord::Base.connection_config[:host]
  p "Using host: #{host}"
end
