## MAIN CALL
jQuery ->
  # Load tooltips
  $(document).tooltip()

  # On selection of "No, ..." to download_maas_app
  $("input[name='download_maas_app']").click ->
    if $("#download_maas_app_3").prop("checked") or $("#download_maas_app_4").prop("checked") or $("#download_maas_app_5").prop("checked")
      $("#reasons_for_not_downloading").show()
    else
      $("input[name='reasons_for_not_downloading[]']").attr('checked',false)
      $("#reasons_for_not_downloading").hide()

  # Create Experiment Form SUMBIT
  $("#create_experiment_resident").on 'submit', () ->
    # Validations
    # if download_maas_app not picked
    if not $("input[name='download_maas_app']:radio:checked").val()
      $("#download_maas_app_error").show().delay(5000).fadeOut()
      $(window).scrollTop($('#download_maas_app_error').offset().top)
      return false

    # if download_maas_app = No, and, no reason selected
    if ($("#download_maas_app_3").prop("checked") or $("#download_maas_app_4").prop("checked") or $("#download_maas_app_5").prop("checked")) and $("input[name='reasons_for_not_downloading[]']:checked").length == 0
      $("#reasons_for_not_downloading_error").show().delay(5000).fadeOut()
      $(window).scrollTop($('#reasons_for_not_downloading_error').offset().top)
      return false

    # if all importance_of_features are not checked
    if $("input[name^=importance_of_features]:checked").length.toString() != $("#importance_of_features_count").val()
      $("#important_features_error").show().delay(5000).fadeOut()
      $(window).scrollTop($('#important_features_error').offset().top)
      return false

  ## Company - 1 ##
  # Plan name #
  # On selection of payg/weekly/monthly plans
  $("input[name='picked_plan']").click ->
    $("#number_of_trips_section").toggle(!$("#picked_plan_4").prop("checked"))
    # Fill plan name
    $("#chosen_plan_type").html($(this).data("name"))

  # Form SUBMIT (i.e. Table Design) #
  $("#company_1_plans").on 'submit', () ->
    # Validations
    # if picked_plan not picked
    if not $("input[name='picked_plan']:radio:checked").val()
      $("#select_plans_error").show().delay(5000).fadeOut()
      $(window).scrollTop($('#select_plans_error').offset().top)
      return false

    # if picked_plan = 1/2/3, but all number_of_trips are not filled
    if $("input[name='picked_plan']:radio:checked").val() != "4"
      if not $("#number_of_trips_pt").val() or not $("#number_of_trips_bs").val() or not $("#number_of_trips_tx").val() or not $("#number_of_trips_cs").val()
        $("#number_of_trips_error").show().delay(5000).fadeOut()
        $(window).scrollTop($('#number_of_trips_error').offset().top)
        return false

  ## Finish Experiment Section ##
  $("#send_user_email").click ->
    email = $("#user_email").val()
    if email
      request = $.get("update_user_email", { email: email })
      request.success (data) ->
        $("#send_user_email").attr("disabled", true)
        return false

  # $("input[class='final_page_choice']").click ->
  #   key = $(this).attr("name")
  #   value = $(this).val()

  #   if key == "test_maas_app"
  #     if value == "1"
  #       $("#feedback_maas_app_section").show()
  #     else
  #       $("#feedback_maas_app_section").hide()

  #   request = $.get("update_final_page_choices", { key: key, value: value })
  #   request.success (data) ->
  #     return false

  ## Company Choice ##
  $("#company_choice").on 'submit', () ->
    if not $("input[name='most_liked_company']:checked").val() or not $("input[name='least_liked_company']:checked").val()
      $("#company_choice_error").show().delay(5000).fadeOut()
      $(window).scrollTop($('#company_choice_error').offset().top)
      return false

  ## Landing Page - MANCHESTER _only_ ##
  $("#begin_survey").on 'submit', () ->
    if $("#rn_flag").val()
      if $("#user_screen_smart_phone").val() and $("#user_screen_age").val() and 
      $("#user_screen_sex").val() and ($("#user_screen_licence_car").is(":checked") or 
      $("#user_screen_licence_motorcycle").is(":checked") or $("#user_screen_licence_none").is(":checked"))
        console.log "TRUE"
      else
        alert("Kindly answer all the screening questions before starting the survey ...")
        return false
