# load custom libraries here since we do not want to add the entire lib directory to autoload_paths

# used by cron jobs
# require "#{Rails.root}/lib/alarm_manager"
# require "#{Rails.root}/lib/general_battery_stats"
# require "#{Rails.root}/lib/notification_verifier"
# require "#{Rails.root}/lib/push_notification_sender"
# require "#{Rails.root}/lib/update_user_stats"
# require "#{Rails.root}/lib/user_classifier"
# require "#{Rails.root}/lib/stops_processor"
# require "#{Rails.root}/jobs/detectStops"
# require "#{Rails.root}/lib/gprmc_parser"
# require "#{Rails.root}/lib/admin_constraint"
# require "#{Rails.root}/tools/get_all_raw_data.rb"
# require "#{Rails.root}/tools/get_stops_summary.rb"
# require "#{Rails.root}/tools/happy_analysis.rb"
# require "#{Rails.root}/tools/get_travel_summary.rb"
# require "#{Rails.root}/tools/remap_old_happy_answers.rb"
