require 'raven'

Raven.configure do |config|
	config.environments = %w[ staging production ]
  config.dsn = 'https://80870626df20473883d86d008aadea2b:0bf30da6985c4575901fbbffdae7d317@sentry.fmsurvey.sg/5'
end