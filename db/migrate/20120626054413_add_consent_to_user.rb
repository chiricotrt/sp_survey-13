class AddConsentToUser < ActiveRecord::Migration
  def self.up
    add_column :users, :consent, :boolean, :default => false
  end

  def self.down
    remove_column :users, :consent
  end
end
