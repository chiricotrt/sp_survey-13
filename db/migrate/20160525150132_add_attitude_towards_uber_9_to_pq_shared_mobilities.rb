class AddAttitudeTowardsUber9ToPqSharedMobilities < ActiveRecord::Migration
  def change
    add_column :pq_shared_mobilities, :attitude_towards_uber_9, :integer
  end
end
