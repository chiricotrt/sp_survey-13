class AddHourlyAndDailyRentalToPqSharedMobilities < ActiveRecord::Migration
  def change
    add_column :pq_shared_mobilities, :hourly_rental, :integer, default: 0
    add_column :pq_shared_mobilities, :daily_rental, :integer, default: 0
  end
end
