class AddSeededStatedPreferenceExperimentIdToStatedPreferenceExperiments < ActiveRecord::Migration
  def change
    add_column :stated_preference_experiments, :seeded_stated_preference_experiment_id, :integer
  end
end
