class AddAdditionalAttitudeTowardsMaasColumnsToSpAttitudesAndImpact < ActiveRecord::Migration
  def change
    add_column :sp_attitudes_and_impacts, :attitude_towards_maas_8, :integer
    add_column :sp_attitudes_and_impacts, :attitude_towards_maas_9, :integer
    add_column :sp_attitudes_and_impacts, :attitude_towards_maas_10, :integer
  end
end
