class CreatePlansTasks < ActiveRecord::Migration
  def change
    create_table :plans_tasks do |t|
      t.integer :plan_id
      t.integer :task_id
    end
  end
end
