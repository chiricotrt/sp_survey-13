class AddLabelToPlans < ActiveRecord::Migration
  def change
    add_column :plans, :label, :string
  end
end
