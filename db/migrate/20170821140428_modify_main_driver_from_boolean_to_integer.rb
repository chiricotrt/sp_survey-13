class ModifyMainDriverFromBooleanToInteger < ActiveRecord::Migration
  def change
    remove_column :pre_survey_private_mobility_characteristics, :main_driver
    add_column :pre_survey_private_mobility_characteristics, :main_driver, :integer
  end
end
