class AddOtherPassNameToPublicTransportTable < ActiveRecord::Migration
  def change
    add_column :pre_survey_public_transport_characteristics, :other_pass_name, :string
  end
end
