class AddOtherJourneyPlannerToJourneyPlannerTable < ActiveRecord::Migration
  def change
    add_column :pre_survey_journey_planner_characteristics, :other_journey_planner_name, :string
  end
end
