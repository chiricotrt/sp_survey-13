class AddFlexibleToSpPlans < ActiveRecord::Migration
  def change
    add_column :sp_plans, :flexible, :boolean, default: false
  end
end
