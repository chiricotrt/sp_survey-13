class AddPlansToSpExperiments < ActiveRecord::Migration
  def change
    add_column :sp_experiments, :plans, :text
  end
end
