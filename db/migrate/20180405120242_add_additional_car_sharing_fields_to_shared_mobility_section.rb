class AddAdditionalCarSharingFieldsToSharedMobilitySection < ActiveRecord::Migration
  def change
    add_column :pre_survey_shared_mobility_characteristics, :frequency_of_car_sharing, :integer
    add_column :pre_survey_shared_mobility_characteristics, :cost_of_shared_vehicle, :float
    add_column :pre_survey_shared_mobility_characteristics, :aware_of_ride_sharing, :integer
  end
end
