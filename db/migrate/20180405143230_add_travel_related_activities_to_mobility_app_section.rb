class AddTravelRelatedActivitiesToMobilityAppSection < ActiveRecord::Migration
  def change
    add_column :pre_survey_journey_planner_characteristics, :travel_related_activities_1, :integer
    add_column :pre_survey_journey_planner_characteristics, :travel_related_activities_2, :integer
    add_column :pre_survey_journey_planner_characteristics, :travel_related_activities_3, :integer
    add_column :pre_survey_journey_planner_characteristics, :travel_related_activities_4, :integer
    add_column :pre_survey_journey_planner_characteristics, :travel_related_activities_5, :integer
    add_column :pre_survey_journey_planner_characteristics, :travel_related_activities_6, :integer
    add_column :pre_survey_journey_planner_characteristics, :travel_related_activities_7, :integer
    add_column :pre_survey_journey_planner_characteristics, :travel_related_activities_8, :integer
  end
end
