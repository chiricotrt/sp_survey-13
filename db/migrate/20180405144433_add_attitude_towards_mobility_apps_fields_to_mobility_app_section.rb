class AddAttitudeTowardsMobilityAppsFieldsToMobilityAppSection < ActiveRecord::Migration
  def change
    add_column :pre_survey_journey_planner_characteristics, :attitude_towards_mobility_apps_1, :integer
    add_column :pre_survey_journey_planner_characteristics, :attitude_towards_mobility_apps_2, :integer
    add_column :pre_survey_journey_planner_characteristics, :attitude_towards_mobility_apps_3, :integer
    add_column :pre_survey_journey_planner_characteristics, :attitude_towards_mobility_apps_4, :integer
    add_column :pre_survey_journey_planner_characteristics, :attitude_towards_mobility_apps_5, :integer
    add_column :pre_survey_journey_planner_characteristics, :attitude_towards_mobility_apps_6, :integer
  end
end
