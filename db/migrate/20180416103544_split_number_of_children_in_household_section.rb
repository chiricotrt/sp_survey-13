class SplitNumberOfChildrenInHouseholdSection < ActiveRecord::Migration
  def change
    rename_column :pre_survey_household_characteristics, :number_of_children, :number_of_children_5
    add_column :pre_survey_household_characteristics, :number_of_children_6_12, :integer
    add_column :pre_survey_household_characteristics, :number_of_children_13_17, :integer
  end
end
