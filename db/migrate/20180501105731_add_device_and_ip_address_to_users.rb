class AddDeviceAndIpAddressToUsers < ActiveRecord::Migration
  def change
    add_column :users, :device, :string
    add_column :users, :ip_address, :string
  end
end
