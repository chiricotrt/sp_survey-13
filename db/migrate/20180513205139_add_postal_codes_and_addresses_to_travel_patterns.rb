class AddPostalCodesAndAddressesToTravelPatterns < ActiveRecord::Migration
  def change
    add_column :pre_survey_travel_patterns, :home_postcode, :text
    add_column :pre_survey_travel_patterns, :home_address, :text
    add_column :pre_survey_travel_patterns, :work_postcodes, :text
    add_column :pre_survey_travel_patterns, :work_addresses, :text
    add_column :pre_survey_travel_patterns, :shop_postcodes, :text
    add_column :pre_survey_travel_patterns, :shop_addresses, :text
    add_column :pre_survey_travel_patterns, :leisure_postcodes, :text
    add_column :pre_survey_travel_patterns, :leisure_addresses, :text
  end
end
  