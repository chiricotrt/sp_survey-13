require 'test_helper'

describe UserSessionsController do
	fixtures(:users)

	before do
		@user = users(:one)
		@session = UserSession.create(@user)
	end

  describe "POST :create" do
  	describe "with valid credentials (email)" do
	    before do
	      post :create, user_session: { username: @user.email, password: "1234"}
	    end

	    it "creates a new user session and redirects user to pages root path" do
	    	assert_equal(assigns(:user_session).persisted?, true)
				must_redirect_to pages_root_path
	    end
  	end

    describe "with valid credentials (username)" do
      before do
        post :create, user_session: { username: @user.username, password: "1234"}
      end

      it "creates a new user session and redirects user to pages root path" do
        assert_equal(assigns(:user_session).persisted?, true)
        must_redirect_to pages_root_path
      end
    end

  	describe "with invalid credentials" do
	    before do
	      post :create, user: {username: @user.email, password: "lorem_ipsum"}
	    end

	    it "presents error messages and redirects user to pages root path" do
	    	assigns(:user_session).errors.wont_be_empty
	    	must_redirect_to pages_root_path
	    end
  	end
  end

  describe "POST :change_timezone" do
  	before do
  		@old_tz = @user.time_zone
  		ApplicationController.any_instance.stubs(:current_user).returns(@user)
  		post :change_timezone, user: {time_zone: "Eastern Time (US & Canada)"}
  	end

  	it "changes the user time zone" do
  		assert_not_equal(@old_tz, @user.reload.time_zone)
  		must_redirect_to root_path
  	end
  end

  describe "DELELE :destroy" do
  	before do
  		ApplicationController.any_instance.stubs(:current_user_session).returns(@session)
  		delete :destroy
  	end

  	it "destroys the session, logging the user out" do
  		assigns(:user_session).destroyed?.must_equal true
  		must_redirect_to pages_newuser_path
  	end
  end
end

